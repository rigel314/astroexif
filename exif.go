package main

import (
	"fmt"
	"log"
	"reflect"

	"github.com/dsoprea/go-exif/v3"
	jis "github.com/dsoprea/go-jpeg-image-structure/v2"
)

func ParseFile(path string) *jis.SegmentList {
	mc, err := jis.NewJpegMediaParser().ParseFile(path)
	if err != nil {
		log.Fatalf("failed jis.NewJpegMediaParser().ParseFile(%s): %v", path, err)
	}

	ex, _, err := mc.Exif()
	if err != nil {
		log.Fatalf("failed mcin.Exif() for path %q: %v", path, err)
	}
	fmt.Println("found exif data on", path)
	for _, v := range ex.DumpTags() {
		x, err := v.Value()
		if err != nil {
			log.Fatalf("failed exin.DumpTags().[].v.Value() for path %q: %v", path, err)
		}
		verboseFmtPrintf("\t%s: %+v\n", v.TagName(), x)
		if v.TagName() == "SceneType" {
			log.Printf("%+v", v)
		}
	}

	return mc.(*jis.SegmentList)
}

func ApplyPreset(ec []exifComponent, sl *jis.SegmentList) {
	ib, err := sl.ConstructExifBuilder()
	if err != nil {
		log.Fatalf("failed sl.ConstructExifBuilder(): %v", err)
	}
	for _, v := range ec {
		// log.Printf("about to set %q to %+v", v.Name, v.Value)
		err := ib.SetStandardWithName(v.Name, v.Value)
		if err != nil {
			log.Fatalf("failed setting %q to %v: %v", v.Name, v.Value, err)
		}
	}
	err = sl.SetExif(ib)
	if err != nil {
		log.Fatalf("failed applying exif changes to in memory object: %v", err)
	}
}

func BuilderTagPath(bt *exif.BuilderTag) string {
	return reflect.ValueOf(bt).Elem().FieldByName("ifdPath").String()
}
