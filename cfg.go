package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"reflect"

	"github.com/dsoprea/go-exif/v3"
	exifcommon "github.com/dsoprea/go-exif/v3/common"
)

type exifComponent struct {
	Name  string
	Value any
}
type cfg struct {
	Presets map[string][]exifComponent
}

var configPaths = []string{
	"astroexif.json",
	filepath.Join(os.Getenv("HOME"), ".config", "astroexif.json"),
}

func ParseConfig() *cfg {
	ret := &cfg{
		Presets: make(map[string][]exifComponent),
	}

	var f *os.File
	var err error
	for _, v := range configPaths {
		f, err = os.Open(v)
		if err == nil {
			break
		}
		defer f.Close()
	}
	if err != nil {
		log.Println("continuing without config file for presets")
		return ret
	}

	jDec := json.NewDecoder(f)
	err = jDec.Decode(ret)
	if err != nil {
		log.Fatalf("failed parsing config file %q: %v", f.Name(), err)
	}

	return ret
}

// reminder on types: https://exiftool.org/TagNames/EXIF.html

func ValidateConfig(cfg *cfg) bool {
	ret := true

	// load list of standard tags
	ti := exif.NewTagIndex()
	exif.LoadStandardTags(ti)

	// validate values of each entry in the config file
	for presetName, presetValues := range cfg.Presets { // for each preset entry
		for i, cfgVal := range presetValues { // for each element in this preset entry
			var messages []string

			t, err := ti.GetWithName(exifcommon.IfdStandardIfdIdentity, cfgVal.Name)
			if err != nil {
				log.Printf("preset %q contains invalid name %q: %v", presetName, cfgVal.Name, err)
				ret = false
			}

			resolved := false
		typeloop:
			for _, t := range t.SupportedTypes {
				// log.Printf("%q/%q: %+v\n", presetName, cfgVal.Name, t)
				switch t {
				case exifcommon.TypeByte, exifcommon.TypeShort, exifcommon.TypeLong:
					val := reflect.ValueOf(cfgVal.Value)
					if val.Kind() != reflect.Slice {
						messages = append(messages, fmt.Sprintf("preset %q/%q can't be %v because it's (%v) instead of an array", presetName, cfgVal.Name, t, val.Kind()))
						continue
					}

					var resultVal reflect.Value
					switch t {
					case exifcommon.TypeByte:
						var x []byte
						resultVal = reflect.New(reflect.TypeOf(x))
					case exifcommon.TypeShort:
						var x []uint16
						resultVal = reflect.New(reflect.TypeOf(x))
					case exifcommon.TypeLong:
						var x []uint32
						resultVal = reflect.New(reflect.TypeOf(x))
					}

					for i := 0; i < val.Len(); i++ {
						elem := val.Index(i).Elem()
						if elem.Kind() != reflect.Float64 {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because it's (%v) instead of a number", presetName, cfgVal.Name, i, t, val.Kind()))
							continue typeloop
						}

						if elem.Float() < 0 {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because it contains a negative value", presetName, cfgVal.Name, i, t))
							continue typeloop
						}

						var intFloat float64
						switch t {
						case exifcommon.TypeByte:
							intFloat = float64(byte(elem.Float()))
						case exifcommon.TypeShort:
							intFloat = float64(uint16(elem.Float()))
						case exifcommon.TypeLong:
							intFloat = float64(uint32(elem.Float()))
						}

						if elem.Float() != intFloat {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because it contains a non-integer or out of range value", presetName, cfgVal.Name, i, t))
							continue typeloop
						}

						switch t {
						case exifcommon.TypeByte:
							x := byte(elem.Float())
							resultVal.Elem().Set(reflect.Append(resultVal.Elem(), reflect.ValueOf(x)))
						case exifcommon.TypeShort:
							x := uint16(elem.Float())
							resultVal.Elem().Set(reflect.Append(resultVal.Elem(), reflect.ValueOf(x)))
						case exifcommon.TypeLong:
							x := uint32(elem.Float())
							resultVal.Elem().Set(reflect.Append(resultVal.Elem(), reflect.ValueOf(x)))
						}
					}

					resolved = true
					presetValues[i].Value = resultVal.Elem().Interface()
					break typeloop

				case exifcommon.TypeSignedLong, exifcommon.TypeFloat, exifcommon.TypeDouble:
					val := reflect.ValueOf(cfgVal.Value)
					if val.Kind() != reflect.Slice {
						messages = append(messages, fmt.Sprintf("preset %q/%q can't be %v because it's (%v) instead of an array", presetName, cfgVal.Name, t, val.Kind()))
						continue
					}

					var resultVal reflect.Value
					switch t {
					case exifcommon.TypeSignedLong:
						var x []int32
						resultVal = reflect.New(reflect.TypeOf(x))
					case exifcommon.TypeFloat:
						var x []float32
						resultVal = reflect.New(reflect.TypeOf(x))
					case exifcommon.TypeDouble:
						var x []float64
						resultVal = reflect.New(reflect.TypeOf(x))
					}

					for i := 0; i < val.Len(); i++ {
						elem := val.Index(i).Elem()
						if elem.Kind() != reflect.Float64 {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because it's (%v) instead of a number", presetName, cfgVal.Name, i, t, val.Kind()))
							continue typeloop
						}

						var intFloat float64
						switch t {
						case exifcommon.TypeSignedLong:
							intFloat = float64(int32(elem.Float()))
						case exifcommon.TypeFloat:
							intFloat = float64(float32(elem.Float()))
						case exifcommon.TypeDouble:
							intFloat = float64(float64(elem.Float()))
						}

						if elem.Float() != intFloat {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because it contains a non-integer or out of range value", presetName, cfgVal.Name, i, t))
							continue typeloop
						}

						switch t {
						case exifcommon.TypeSignedLong:
							x := int32(elem.Float())
							resultVal.Elem().Set(reflect.Append(resultVal.Elem(), reflect.ValueOf(x)))
						case exifcommon.TypeFloat:
							x := float32(elem.Float())
							resultVal.Elem().Set(reflect.Append(resultVal.Elem(), reflect.ValueOf(x)))
						case exifcommon.TypeDouble:
							x := float64(elem.Float())
							resultVal.Elem().Set(reflect.Append(resultVal.Elem(), reflect.ValueOf(x)))
						}
					}

					resolved = true
					presetValues[i].Value = resultVal.Elem().Interface()
					break typeloop

				case exifcommon.TypeRational:
					val := reflect.ValueOf(cfgVal.Value)
					if val.Kind() != reflect.Slice {
						messages = append(messages, fmt.Sprintf("preset %q/%q can't be %v because it's (%v) instead of an array", presetName, cfgVal.Name, t, val.Kind()))
						continue
					}

					var resultVal []exifcommon.Rational

					for i := 0; i < val.Len(); i++ {
						elem := val.Index(i).Elem()
						if elem.Kind() != reflect.Map {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because it's (%v) instead of an object", presetName, cfgVal.Name, i, t, val.Kind()))
							continue typeloop
						}
						num := elem.MapIndex(reflect.ValueOf("Numerator")).Elem()
						den := elem.MapIndex(reflect.ValueOf("Denominator")).Elem()
						if num.Kind() != reflect.Float64 || den.Kind() != reflect.Float64 {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because Numerator and Denominator are not both float64s (%v,%v)", presetName, cfgVal.Name, i, t, num.Kind(), den.Kind()))
							continue typeloop
						}

						if num.Float() < 0 || den.Float() < 0 {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because it contains negative values", presetName, cfgVal.Name, i, t))
							continue typeloop
						}

						if num.Float() != float64(uint32(num.Float())) || den.Float() != float64(uint32(den.Float())) {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because it contains non-integer or out of range values", presetName, cfgVal.Name, i, t))
							continue typeloop
						}

						resultVal = append(resultVal, exifcommon.Rational{
							Numerator:   uint32(num.Float()),
							Denominator: uint32(den.Float()),
						})
					}

					resolved = true
					presetValues[i].Value = resultVal
					break typeloop

				case exifcommon.TypeSignedRational:
					val := reflect.ValueOf(cfgVal.Value)
					if val.Kind() != reflect.Slice {
						messages = append(messages, fmt.Sprintf("preset %q/%q can't be %v because it's (%v) instead of an array", presetName, cfgVal.Name, t, val.Kind()))
						continue
					}

					var resultVal []exifcommon.SignedRational

					for i := 0; i < val.Len(); i++ {
						elem := val.Index(i).Elem()
						if elem.Kind() != reflect.Map {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because it's (%v) instead of an object", presetName, cfgVal.Name, i, t, val.Kind()))
							continue typeloop
						}
						num := elem.MapIndex(reflect.ValueOf("Numerator")).Elem()
						den := elem.MapIndex(reflect.ValueOf("Denominator")).Elem()
						if num.Kind() != reflect.Float64 || den.Kind() != reflect.Float64 {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because Numerator and Denominator are not both float64s (%v,%v)", presetName, cfgVal.Name, i, t, num.Kind(), den.Kind()))
							continue typeloop
						}

						if num.Float() != float64(uint32(num.Float())) || den.Float() != float64(uint32(den.Float())) {
							messages = append(messages, fmt.Sprintf("preset %q/%q[%d] can't be %v because it contains non-integer or out of range values", presetName, cfgVal.Name, i, t))
							continue typeloop
						}

						resultVal = append(resultVal, exifcommon.SignedRational{
							Numerator:   int32(num.Float()),
							Denominator: int32(den.Float()),
						})
					}

					resolved = true
					presetValues[i].Value = resultVal
					break typeloop

				case exifcommon.TypeAscii:
					val := reflect.ValueOf(cfgVal.Value)
					if val.Kind() != reflect.String {
						messages = append(messages, fmt.Sprintf("preset %q/%q can't be %v because it's (%v) instead of a string", presetName, cfgVal.Name, t, val.Kind()))
						continue
					}

					resolved = true
					presetValues[i].Value = val.String()
					break typeloop

				default:
					messages = append(messages, fmt.Sprintf("preset %q/%q could have type %v which is unsupported by this tool", presetName, cfgVal.Name, t))
					continue typeloop
				}
			}
			if !resolved {
				ret = false
				for _, v := range messages {
					log.Println(v)
				}
			}
		}
	}
	return ret
}
