package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"reflect"
	"runtime/debug"

	"github.com/dsoprea/go-exif/v3"
	exifcommon "github.com/dsoprea/go-exif/v3/common"
	jpegstructure "github.com/dsoprea/go-jpeg-image-structure/v2"
)

var (
	input    = flag.String("input", "", "input filename, required for 'copy' preset")
	output   = flag.String("output", "", "output filename, required")
	preset   = flag.String("preset", "copy", "required, name of preset from astroexif.json, 'copy' is a special value to copy all exif from an input file")
	verbose  = flag.Bool("verbose", false, "be more verbose")
	validate = flag.Bool("validate", false, "validate config")
)

func main() {
	flag.Parse()
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	gitVer := "Uncontrolled"
	if bi, ok := debug.ReadBuildInfo(); ok {
		for _, kv := range bi.Settings {
			if kv.Key == "vcs.revision" {
				gitVer = kv.Value
			}
		}
	}
	verboseLogPrintln("Version:", gitVer)

	cfg := ParseConfig()

	ok := ValidateConfig(cfg)

	if !ok {
		return
	}

	if *validate {
		if ok {
			fmt.Println("looks good")
		}
		return
	}

	sl := ParseFile(*output)
	if *preset == "copy" {
		slin := ParseFile(*input)

		sl.DropExif()

		ibout, err := sl.ConstructExifBuilder()
		if err != nil {
			log.Fatalf("failed input sl.ConstructExifBuilder()")
		}

		CopyTags(ibout, slin)

		err = sl.SetExif(ibout)
		if err != nil {
			log.Fatalf("failed applying input exif changes to in memory object: %v", err)
		}
	}

	pre, ok := cfg.Presets[*preset]
	if !ok {
		if *preset != "copy" {
			log.Fatalf("preset %q not found in config", *preset)
		}
	}
	ApplyPreset(pre, sl)

	// ex, _, err := sl.Exif()
	// if err != nil {
	// 	panic(err)
	// }
	// for _, v := range ex.DumpTags() {
	// 	x, err := v.Value()
	// 	if err != nil {
	// 		log.Println("#########", err)
	// 	}
	// 	fmt.Printf("%s: %+v\n", v.TagName(), x)
	// }

	f, err := os.Create(*output + ".tmp.jpg")
	// f, err := os.CreateTemp(filepath.Dir(*output), filepath.Base(*output)+".*.jpg")
	if err != nil {
		log.Fatalf("failed to open temp output file: %v", err)
	}

	err = sl.Write(f)
	if err != nil {
		log.Fatalf("failed to write temp output file %q: %v", f.Name(), err)
	}
	f.Close()

	sl = ParseFile(*output + ".tmp.jpg")
	ex, _, err := sl.Exif()
	if err != nil {
		panic(err)
	}
	for _, v := range ex.DumpTags() {
		x, err := v.Value()
		if err != nil {
			log.Println("#########", err)
		}
		fmt.Printf("%s: %+v\n", v.TagName(), x)
	}
}

func CopyTags(ibout *exif.IfdBuilder, slin *jpegstructure.SegmentList) {
	ib, err := slin.ConstructExifBuilder()
	if err != nil {
		log.Fatalf("failed slin.ConstructExifBuilder()")
	}
	// ex, _, err := slin.Exif()
	// if err != nil {
	// 	panic(err)
	// }
	// err = ibout.AddTagsFromExisting(ex, nil, nil)
	// if err != nil {
	// 	panic(err)
	// }
	for _, v := range ib.Tags() {
		if v.Value().IsIb() {
			ib, err := exif.GetOrCreateIbFromRootIb(ibout, BuilderTagPath(v))
			if err != nil {
				panic(err)
			}
			err = ib.SetStandardWithName("XPKeywords", []byte("hello"))
			if err != nil {
				panic(err)
			}
			err = ibout.AddChildIb(ib)
			if err != nil {
				panic(fmt.Sprintf("%v: %q", err, BuilderTagPath(v)))
			}
			continue
		}
		typ := exifcommon.TagTypePrimitive(reflect.ValueOf(v).Elem().FieldByName("typeId").Uint())
		log.Println(typ, v.Value())
		if typ == exifcommon.TypeUndefined {
			log.Println("here")
			continue
		}
		ibout.Set(v)
	}
}
