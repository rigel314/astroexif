astroexif
======

## About
This tool has two purposes:
* ability to copy exif metadata from one jpeg to another
  * mainly because some picture editing tools I use drop the exif data for some reason
* fill in missing exif metadata from a few different configurable presets
  * mainly because when I do astrophotography, my telescope doesn't tell my camera anything about focal length or aperture - and I sometimes use a barlow to artificially increase my apparent focal length or take wide angle pictures through my finder scope.
