#!/bin/bash

function run() {
	echo "$@"
	"$@"
}

if [[ -z "$1" || -z "$2" ]]; then
	echo "Usage: $0 fromfile tofile"
	echo "  Erases all meta information from tofile image, then copy EXIF tags from fromfile."
	exit 1
fi

run exiftool -all= -tagsFromFile "$1" -exif:all "$2"
