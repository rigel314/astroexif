#!/bin/bash

function run() {
	echo "$@"
	"$@"
}

if [[ -z "$1" || -z "$2" ]]; then
	echo "Usage: $0 presetfile file [args...]"
	echo "  Uses presetFile as an exiftool ARGFILE, operates on file, also passes args into exiftool."
	exit 1
fi

presetFile="$1"
file="$2" 

shift
shift

run exiftool -@ "$presetFile" "$@" "$file"
