#!/bin/bash

function run() {
	echo "$@"
	"$@"
}

if [[ -z "$1" ]]; then
	echo "Usage: $0 file"
	echo "  Erases all gps meta information from file image."
	exit 1
fi

run exiftool "-gps:all=" "$1"
