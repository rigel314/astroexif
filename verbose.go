package main

import (
	"fmt"
	"log"
)

func verboseLogPrintln(a ...any) {
	if *verbose {
		log.Println(a...)
	}
}
func verboseLogPrintf(fmt string, a ...any) {
	if *verbose {
		log.Printf(fmt, a...)
	}
}
func verboseFmtPrintf(f string, a ...any) {
	if *verbose {
		fmt.Printf(f, a...)
	}
}
